<?php get_header('page') ?>
<?php
 $args = array(
     'post_type'=> 'create_posttype_services',
     'orderby'    => 'ID',
     'post_status' => 'publish',
     'order'    => 'DESC',
     'posts_per_page' => -1 //every post will shown here
 );
 $result = new WP_Query( $args );
 if ($result->have_posts())
 {
      while ($result->have_posts()) : $result->the_post();
    //   $img = wp_get_attachment_image_src(get_post_thumbnail_id($query->ID),'full');    
 ?>

          <!-- <img src="<?php echo $img[0]; ?>" alt="" /> -->
          <?php echo wp_trim_words( get_the_title(), 6, '...' ); ?>
          <?php echo get_the_date( 'jS F, Y' ); ?>
          <a href="<?php the_permalink(); ?>">Read More</a>


 <?php  endwhile;  
 } else echo "No Data Found";
 wp_reset_postdata();
 ?>
<?php get_footer() ?>

