<?php 
/**
 * Template name: Pricing page template
 */
get_header('page');
?>
    <!-- Pricing Plan Start -->
    <div class="container-fluid pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-6 text-center mb-5">
                    <small class="d-inline bg-primary text-white text-uppercase font-weight-bold px-1">Our Pricing Plan</small>
                    <h1 class="mt-2 mb-3"><?=get_field('pricing-section-1-head')?></h1>
                    <h4 class="font-weight-normal text-muted mb-4"><?=get_field('pricing-section-1-title')?></h5>
                    <a href="<?=get_field('pricing-section-1-button')?>" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Contact Now</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mb-5">
                    <div class="d-flex flex-column align-items-center justify-content-center bg-primary p-4">
                        <h3>Basic</h3>
                        <h1 class="display-4 mb-0">
                            <small class="align-top"
                                style="font-size: 22px; line-height: 45px;">$</small><?=get_field('pricing-section-2-basic')?><small
                                class="align-bottom" style="font-size: 16px; line-height: 40px;">/
                                Mo</small>
                        </h1>
                    </div>
                    <div class="border border-top-0 d-flex flex-column align-items-center py-4">
                        <?=get_field('pricing-section-2-basic-content')?>
                        <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold my-2">Order Now</a>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="d-flex flex-column align-items-center justify-content-center bg-primary p-4">
                        <h3>Premium</h3>
                        <h1 class="display-4 mb-0">
                            <small class="align-top"
                                style="font-size: 22px; line-height: 45px;">$</small><?=get_field('pricing-section-2-premium')?><small
                                class="align-bottom" style="font-size: 16px; line-height: 40px;">/
                                Mo</small>
                        </h1>
                    </div>
                    <div class="border border-top-0 d-flex flex-column align-items-center py-4">
                    <?=get_field('pricing-section-2-premium-content')?>

                        <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold my-2">Order Now</a>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="d-flex flex-column align-items-center justify-content-center bg-primary p-4">
                        <h3>Business</h3>
                        <h1 class="display-4 mb-0">
                            <small class="align-top"
                                style="font-size: 22px; line-height: 45px;">$</small><?=get_field('pricing-section-2-business')?><small
                                class="align-bottom" style="font-size: 16px; line-height: 40px;">/
                                Mo</small>
                        </h1>
                    </div>
                    <div class="border border-top-0 d-flex flex-column align-items-center py-4">
                    <?=get_field('pricing-section-2-business-content')?>


                        <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold my-2">Order Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pricing Plan End -->


<?php get_footer() ?>
 