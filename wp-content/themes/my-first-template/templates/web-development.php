<?php 

/**
 * Template name: development page template
 */
get_header('page');

?>
 <div class="row">
                <div class="col-md-4 mb-5">
                    <div class="d-flex">
                        <i class="fa fa-3x fa-laptop-code text-primary mr-4"></i>
                        <div class="d-flex flex-column">
                            <h4 class="font-weight-bold mb-3">Web Design</h4>
                            <p>Et kasd justo clita amet kasd, vero amet vero eos kasd diam justo, ipsum diam sed elitr
                                erat</p>
                            <a class="font-weight-semi-bold" href="">Read More <i
                                    class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
    </div>
    <?php get_footer() ?>