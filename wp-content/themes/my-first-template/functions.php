<?php 
// Our custom post type function
function create_posttype_services() {
    register_post_type( 'services',
        array(
            'labels' => array(
                'name' => __( 'Services' ),
                'singular_name' => __( 'Services' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'service'),
            'show_in_rest' => true,
        )
    );
}
add_action( 'init', 'create_posttype_services' );



add_action( 'init', 'create_service_taxonomy', 0 );
function create_service_taxonomy() {
    $labels = array(
        'name' => __( 'Service Categories'),
        'singular_name' => __( 'Service Category'),
        'search_items' =>  __( 'Search Service Category' ),
        'all_items' => __( 'All Service Categories' ),
        'parent_item' => __( 'Parent Service Category' ),
        'parent_item_colon' => __( 'Parent Service Category:' ),
        'edit_item' => __( 'Edit Service Category' ),
        'update_item' => __( 'Update Service Category' ),
        'add_new_item' => __( 'Add New Service Category' ),
        'new_item_name' => __( 'New Service Category Name' ),
        'menu_name' => __( 'Service Categories' ),
    );
    register_taxonomy('service_categories', [
        'services'
    ], [
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => false,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'service-category' ),
    ]);
}
