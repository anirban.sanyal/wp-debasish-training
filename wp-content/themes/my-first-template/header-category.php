<?php
    get_header();
    $term_id = get_queried_object_id();
    $term = get_term($term_id);
?>

<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
    <h1 class="display-4 text-white mb-3 mt-0 mt-lg-5"><?=$term->name; ?></h1>
    <div class="d-inline-flex text-white">
        <p class="m-0"><a class="text-white" href="<?=home_url(); ?>">Home</a></p>
        <p class="m-0 px-2">/</p>
        <p class="m-0"><?=$term->name; ?></p>
    </div>
</div>
<!-- Page Header Start -->
