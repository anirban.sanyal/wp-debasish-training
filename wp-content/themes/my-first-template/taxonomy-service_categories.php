<?php
    get_header('category');
    $queriedObject = get_queried_object(); //fetching the object created in page
    $queriedObjectId = get_queried_object_id(); //fetching the object id created in page
    $isItTerm = is_tax('service_categories', $queriedObjectId);

    $childTerms = get_term_children($queriedObjectId, 'service_categories'); //fetching the taxonomy child id's from the "service_categories" taxonomy
    // $post = get_post();
    // print_r($post);
    // die;

?>
    Childs :
    <ul>
        <?php foreach($childTerms as $childTerm):
            $term = get_term($childTerm); //fetching the child data(object) using child id
            // print_r($term);
        ?>
            <li>
                <a href="<?=get_term_link($term); ?>"><?=$term->name; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    Services :
    <ul>
        <?php
        $queryArgs = 
        [
            'post_type' => 'services',
            'tax_query' => 
            [
                [
                    'taxonomy' => 'service_categories', 
                    'terms' => $queriedObjectId,
                    'include_children' => false
                ],
            ],
        ];
        $wpQuery = new WP_Query($queryArgs); // fetch post's under taxonomy categories;
        // echo "<pre/>";
        // print_r($wpQuery);
        // die;
        $posts = $wpQuery->get_posts();
        foreach($posts as $post):?>
        <li>
            <a href="<?=get_permalink($post); ?>"><?=$post->post_title; ?></a>
        </li>
        <?php endforeach; ?>
    </ul>
<?php
    get_footer();